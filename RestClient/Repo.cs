﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestClient
{
    class Repo
    {
        public String id;
        public String name;
        public String forks;
        public String watchers;
        public String open_issues;
        public String default_branch;


        public String GetName()
        {
            return this.name;
        }
    }
}
