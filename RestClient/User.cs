﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestClient
{
    class User
    {
        public String name;
        public String location;
        public String company;
        public String public_repos;
        public String following;

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return "name: " + name 
                + "\nlocation: " + location
                + "\ncompany: " + company
                + "\npublic_repos: " + public_repos
                + "\nfollowing: " + following;
        }
    }
}
