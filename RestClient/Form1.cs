﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace RestClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            //Nic nie robimy
        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {
            //Nic nie robimy
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!search.Text.Equals(""))
            {
                RestClientService rc = new RestClientService();
                String getUserInfo = @"https://api.github.com/users/" + search.Text;
                rc.endpoint = getUserInfo;
                result.Text = JsonConvert.DeserializeObject<User>(rc.MakeRequest()).ToString();

                String getUserRepos = @"https://api.github.com/users/" + search.Text + "/repos";
                rc.endpoint = getUserRepos;
                List<Repo> reposList = JsonConvert.DeserializeObject<List<Repo>>(rc.MakeRequest());
                repos.Text = "";
                foreach (Repo repo in reposList)
                {
                    repos.Text = repos.Text + repo.GetName() + "\n";
                }

                String getUserFollowers = @"https://api.github.com/users/" + search.Text + "/followers";
                rc.endpoint = getUserFollowers;
                List<Follower> followersList = JsonConvert.DeserializeObject<List<Follower>>(rc.MakeRequest());


                followers.Text = "";
                foreach (Follower follower in followersList)
                {
                    followers.Text = followers.Text + follower.GetLogin() + "\n";
                }
                
            
            } else
            {
                result.Text = "";
                repos.ResetText();
                followers.ResetText();
            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            //Nic nie robimy
        }

        private void label1_Click(object sender, EventArgs e)
        {
            //Nic nie robimy
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Nic nie robimy
        }
    }
}
