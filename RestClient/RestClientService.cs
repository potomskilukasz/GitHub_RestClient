﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace RestClient
{
    public enum HttpVerb{
        GET,POST,PUT,DELETE
    }

    class RestClientService
    {
        public String endpoint { get; set; }
        public HttpVerb httpMethod { get; set; }

        public RestClientService()
        {
            httpMethod = HttpVerb.GET;
            endpoint = string.Empty; 

        }

        public String MakeRequest()
        {
            String result = string.Empty;

            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(endpoint);
            request.Method = httpMethod.ToString();
            request.UserAgent = "Mozilla/4.78";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (!response.StatusCode.Equals(HttpStatusCode.OK))
                {
                    result = "Serwer zwrocił błąd: " + response.StatusCode + " " + response.StatusDescription;
                }
                using (Stream responseStream = response.GetResponseStream())
                {
                    if (responseStream != null)
                    {
                        using (StreamReader streamReader = new StreamReader(responseStream))
                        {
                            result = streamReader.ReadToEnd();
                        }
                    }
                }
            }

            
            return result ;
        }
    }
}
